import pandas as pd
import numpy as np
if __name__ == '__main__':
    video_data = pd.read_csv('annotated_data/all_dances_matt_format.csv')

    reorganized = video_data[['id','frame','x','y']]

    reorganized.columns = ['id','t','x','y']

    reorganized.drop(columns=['id']).to_csv("annotated_data/all_cances_griffin_format.csv")

    unique_ids = video_data['id'].unique()

    video_lengths = []

    print("frames per seconds: 30")

    print("dance_id, number_of_frames")

    for i, id in enumerate(unique_ids):
        data_set = reorganized[reorganized['id'] == id]
        data_set = data_set.drop(columns=['id'])
        data_set.to_csv(f"annotated_data/WaggleDance_27_{id}.csv", index=False)
        print(i, len(data_set))

