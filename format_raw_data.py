import numpy as np
import pandas as pd
import json
from bs4 import BeautifulSoup
import cv2
from moviepy.editor import *
import itertools
import operator



if __name__ == '__main__':

    image_folder = 'images'

    file_names = ['1-2.xml','2.xml','3.xml','4.xml','5.xml','6.xml']

    # file_names = ['1-2.xml']

    file_annotations = []

    video_file_locations = ['raw_videos/video1.mp4', 'raw_videos/video2.mp4', 'raw_videos/video3.mp4', 'raw_videos/video4.mp4',
                            'raw_videos/video5.mp4', 'raw_videos/video6.mp4', "merged.mp4"]
    video_lengths = []
    video_files = []

    for file in video_file_locations:
        cap = cv2.VideoCapture(file)
        length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        video_lengths.append(length)

    current_track_num = 0



    for nth_file, file in enumerate(file_names):

        compiled_file = None

        with open(f'raw_annotations/{file}', 'r') as f:
            data = f.read()

        Bs_data = BeautifulSoup(data, "xml")

        tracks = Bs_data.find_all(name='track')

        entries = []

        for track in tracks:
            track_id = int(track.attrs['id'])
            points = track.find_all(name="points")
            for point in points:
                broken_ponits = point.attrs['points'].split(';')
                for j, sub_point in enumerate(broken_ponits):
                    x, y = sub_point.split(',')
                    entries.append( [track_id, int(point.attrs['frame']), float(x), float(y) ])
                    # break

        pandas_results = pd.DataFrame(entries)
        pandas_results.columns = ['id', 'frame', 'x', 'y']
        pandas_results['clip_number'] = nth_file
        pandas_results['net_position'] = np.linalg.norm( pandas_results[ ['x', 'y'] ], axis=1 )

        for id in pandas_results['id'].unique():
            entries = pandas_results[pandas_results['id'] == id]
            movement_changes = entries['net_position'].diff()
            one_block = np.zeros(len(movement_changes))

            one_block[movement_changes != 0] = 1
            blocks = [[j for j, value in it] for key, it in itertools.groupby(enumerate(one_block), key=operator.itemgetter(1))]
            accepted_blocks = []
            for block in blocks:
                add = True
                if len(block) > 10:
                    if one_block[block[0]] == 0:
                        add = False
                if add:
                    accepted_blocks.append(block)

            accepted_entries = [item for sublist in accepted_blocks for item in sublist]

            accepted_entries = entries.iloc[accepted_entries]

            if compiled_file is None:
                compiled_file = accepted_entries
            else:
                compiled_file = pd.concat([compiled_file, accepted_entries])
            file_annotations.append(accepted_entries)


        # vidcap = cv2.VideoCapture("merged.mp4")
        vidcap = cv2.VideoCapture(f"{video_file_locations[nth_file]}")

        success, image = vidcap.read()
        count = 0

        video_writter = None

        video_name = f"annotated_video_{nth_file+1}.mp4"

        height, width = image.shape[:2]
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        fps = 30
        video_writter = cv2.VideoWriter(video_name, fourcc, fps, (width, height))

        colors = [(255,0,0), (0,255,0), (0,0,255), (255,255,0), (0,255,255), (255,0,255), (128,0,0),(128,128,0), (0,128,0), (128,0,128), (0,128,128), (0,0,128), (0,0,0)]

        while success:
            frames = compiled_file[compiled_file['frame'] == count]

            for i in range(len(frames)):
                image = cv2.circle(image, ( int(frames.iloc[i]['x']), int(frames.iloc[i]['y']) ), radius=5, color=colors[0], thickness=-1)
                # image = cv2.circle(image, (int(frames.iloc[i]['x']), int(frames.iloc[i]['y'])), radius=0, color=(0, 0, 255), thickness=-1)

            video_writter.write(image)
            count += 1
            success, image = vidcap.read()

        compiled_file.to_csv(f"movement_output{nth_file+1}.csv")

        cv2.destroyAllWindows()
        video_writter.release()
