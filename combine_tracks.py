import numpy as np
import pandas as pd
import json
from bs4 import BeautifulSoup
import cv2
from moviepy.editor import *
import itertools
import operator


def correct_run_seams(input, length):
    needed_runs = input

    first_entries_list = []
    last_entries_list = []

    clip_ids = []

    for clip_id in range(len(length) - 1):
        first_entries_clip = []
        last_entries_clip = []

        last_frame_num = length[clip_id]

        clip_entries = needed_runs[needed_runs['clip_number'] == clip_id]

        unique_ids = clip_entries['id'].unique()
        clip_ids.append(unique_ids)

        for id in unique_ids:
            clip_entries_for_id = clip_entries[clip_entries['id'] == id]
            last_entries = clip_entries_for_id[clip_entries_for_id['clip_frame'] > last_frame_num - 5]
            if len(last_entries) > 0:
                last_entry = last_entries[['x', 'y']].iloc[-1]
                last_entries_clip.append([last_entry, id])

            first_entries = clip_entries_for_id[clip_entries_for_id['clip_frame'] < 5]

            if len(first_entries) > 0:
                first_entry = first_entries[['x', 'y']].iloc[0]
                first_entries_clip.append([first_entry, id])

        first_entries_list.append(first_entries_clip)
        last_entries_list.append(last_entries_clip)

    entries = []

    matched_ids = []

    for i in range(len(last_entries_list) - 1):
        for j in range(len(last_entries_list[i])):
            for k in range(len(first_entries_list[i + 1])):
                closeness = np.linalg.norm(abs(np.array(last_entries_list[i][j][0]) - np.array(first_entries_list[i + 1][k][0])))
                if closeness < 50:
                    matched_ids.append([last_entries_list[i][j][1], first_entries_list[i + 1][k][1]])
                entries.append([i, j, k, closeness])

    for pair in matched_ids:
        needed_runs['id'][needed_runs['id'] == pair[1]] = pair[0]

    unique_ids = needed_runs['id'].unique()

    for i in range(len(unique_ids)):
        needed_runs['id'][needed_runs['id'] == unique_ids[i]] = i

    # runs 1 and 2 are the same (Timothy accidentally did this bee run twice)
    needed_runs = input[input['id'] != 0]

    unique_ids = needed_runs['id'].unique()

    print(len(unique_ids))

    for i in range(len(unique_ids)):
        needed_runs['id'][needed_runs['id'] == unique_ids[i]] = i

    return needed_runs


if __name__ == '__main__':
    annotation_files = ['movement_output1.csv','movement_output2.csv','movement_output3.csv','movement_output4.csv','movement_output5.csv','movement_output6.csv']

    video_file_locations = ['raw_videos/video1.mp4', 'raw_videos/video2.mp4', 'raw_videos/video3.mp4', 'raw_videos/video4.mp4',
                            'raw_videos/video5.mp4', 'raw_videos/video6.mp4']

    # video_file_locations = ['annotated_video1.mp4','annotated_video2.mp4','annotated_video3.mp4','annotated_video4.mp4','annotated_video5.mp4','annotated_video6.mp4']

    fps = 30

    video_name = "annotated_data/unedited_video.mp4"

    height, width = (720, 1280)

    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')

    video_lengths = []
    video_files = []
    for file in video_file_locations:
        cap = cv2.VideoCapture(file)
        length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        video_lengths.append(length)

        # only uncomment if you want to compile video, this operation is really slow so only do it once
    #     video_files.append(VideoFileClip(file))
    # final = concatenate_videoclips(video_files)
    # final.write_videofile(video_name)

    frame_offset = 0

    compiled_file = None

    for i, file in enumerate(annotation_files):
        pandas_results = pd.read_csv(file)
        pandas_results['clip_frame'] = pandas_results['frame']
        pandas_results['frame'] += frame_offset
        frame_offset += video_lengths[i]

        if compiled_file is None:
            compiled_file = pandas_results
        else:
            id_offset = len(compiled_file['id'].unique())
            pandas_results['id'] += id_offset
            compiled_file = pd.concat([compiled_file, pandas_results])

    compiled_file = correct_run_seams(compiled_file, video_lengths)

    compiled_copy = compiled_file.copy()

    # runs 0 and 1 are the same (Timothy accidentally did this bee run twice)
    # needed_runs = compiled_file[compiled_file['id'] != 0]
    unique_ids = compiled_file['id'].unique()
    for i in range(len(unique_ids)):
        compiled_copy['id'][compiled_file['id'] == unique_ids[i]] = i

    compiled_copy.to_csv("final_video_data.csv")

    video_writter = cv2.VideoWriter("annotated_data/annotated_video.mp4", fourcc, fps, (width, height))

    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (0, 255, 255), (255, 0, 255), (128, 0, 0),
              (128, 128, 0), (0, 128, 0), (128, 0, 128), (0, 128, 128), (0, 0, 128), (0, 0, 0), (64,0,0),(0,64,0),(0,0,64)]

    vidcap = cv2.VideoCapture(video_name)

    success, image = vidcap.read()
    count = 0
    ids_read = []
    while success:
        frames = compiled_copy[compiled_copy['frame'] == count]

        for i in range(len(frames)):
            image = cv2.circle(image, (int(frames.iloc[i]['x']), int(frames.iloc[i]['y'])), radius=5,
                               color=colors[int(frames.iloc[i]['id'])], thickness=-1)
            ids_read.append(int(frames.iloc[i]['id']))
            # image = cv2.circle(image, (int(frames.iloc[i]['x']), int(frames.iloc[i]['y'])), radius=0, color=(0, 0, 255), thickness=-1)

        video_writter.write(image)
        count += 1
        success, image = vidcap.read()

    compiled_copy.to_csv(f"annotated_data/all_dances_matt_format.csv")

    cv2.destroyAllWindows()
    video_writter.release()
